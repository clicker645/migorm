## Installation
```bash
$ go get -u gitlab.com/gozof/migorm
```

## Usage
You must have ```GOPATH``` and ```GOBIN``` configured. If after installing the package in the folder there is no ```gen-migration``` bin file, go to the folder with the package and run ```go install ./...```

The migration list is in ```database/migrate/migrations/migrations_list.go```

##Gen migration file
Your must be in main pkg! Run command in your terminal: ```gen-migration -name=some_name_table```
After go to the database / migration / migration. You will see two files: a list of all the migration and a migration file.
Open the migration file and fill in the structure with the necessary fields. 
You can also specify the name of the table in the database if it was not generated correctly.

##Example
```go
package main

import (
   /...
    . "github.com/dave/jennifer/jen"
   /...
)

func main() {
	needMigrate := flag.Bool("migrate", false, "if true - migrations will be start")
	flag.Parse()

	//Connect to database us gorm
	gormConnect := Connect()

	if *needMigrate == true {
		if err := migorm.New(gormConnect, migrations.GetMigrationsList()).Up(); err != nil {
			log.Fatal(err)
		}
	}

	//...
	//Your code
}
```