package main

import (
	"flag"
	"fmt"
	. "github.com/dave/jennifer/jen"
	"io/ioutil"
	"log"
	"os"
	"regexp"
	"strconv"
	"time"
)

var (
	typeStruct = regexp.MustCompile(`(?Umsi)` + "type ([\\w]+) struct")
)

const (
	PathToMigrationComponents = "./database/migration/migrations/"
)

func main() {
	var (
		FileName              = flag.String("name", "", "This is file name in type: some_table_name")
		RefreshMigrationsList = flag.String("update", "", "If value list - migrations list will be update")
	)

	flag.Parse()

	if *RefreshMigrationsList == "list" {
		MakeMigrationsList()
		log.Println("Migrations list has been updated")

		return
	}

	var (
		CamelCaseStructureName = "Create" + ToCamel((*FileName)+"Table")
		TimeNow                = time.Now().Unix()
		TimeNowString          = strconv.Itoa(int(TimeNow))
		GoFileName             = strconv.Itoa(int(TimeNow)) + "_" + *FileName + ".go"
	)

	f := NewFile("migrations")

	f.Type().Id(CamelCaseStructureName).Struct(
		Id("ID").Uint().Tag(map[string]string{"gorm": "AUTO_INCREMENT;PRIMARY_KEY"}),
		Comment("add your fields"),
		Id("CreatedAt").Uint64().Tag(map[string]string{"gorm": "not null"}),
		Id("UpdatedAt").Uint64(),
	)

	f.Func().Params(
		Id("m").Id("*" + CamelCaseStructureName),
	).Id("TableName").Params().String().Block(Return(Lit(*FileName)))

	f.Func().Params(
		Id("m").Id("*" + CamelCaseStructureName),
	).Id("MigrationId").Params().String().Block(Return(Lit(TimeNowString)))

	if _, err := os.Stat(PathToMigrationComponents); os.IsNotExist(err) {
		if err := os.MkdirAll(PathToMigrationComponents, os.ModePerm); err != nil {
			log.Println(err)
		}
	}

	if err := f.Save(PathToMigrationComponents + GoFileName); err != nil {
		panic(err)
	}

	MakeMigrationsList()

	fmt.Println("Migration successfully created: " + GoFileName)
}

func MakeMigrationsList() {
	var SliceCode []Code

	File := NewFile("migrations")
	File.Id("import (\"gopkg.in/gormigrate.v1\"\n\"gitlab.com/gozof/migorm\"\n)")

	filesMigrations, err := ioutil.ReadDir(PathToMigrationComponents)
	if err != nil {
		panic(err)
	}

	for i := range filesMigrations {
		f, _ := ioutil.ReadFile(PathToMigrationComponents + filesMigrations[len(filesMigrations)-1-i].Name())

		if typeStruct.MatchString(string(f)) {
			result := typeStruct.FindAllStringSubmatch(string(f), -1)
			SliceCode = append(SliceCode, Id("migorm.NewMigration(new("+result[0][1]+")),\n"))
		}
	}

	File.Func().Id("GetMigrationsList").Params().Id("[]*gormigrate.Migration").Block(
		Return().Id("[]*gormigrate.Migration").Block(
			SliceCode...,
		),
	)

	if err := File.Save(PathToMigrationComponents + "migrations_list.go"); err != nil {
		panic(err)
	}

}
