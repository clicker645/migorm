package migorm

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
	"log"
)

type Migrate struct {
	db     *gorm.DB
	list   []*gormigrate.Migration
	migorm *gormigrate.Gormigrate
}

func New(connection *gorm.DB, migrationList []*gormigrate.Migration) *Migrate {
	return &Migrate{
		db:     connection,
		list:   migrationList,
		migorm: gormigrate.New(connection, gormigrate.DefaultOptions, migrationList),
	}
}

//Start migration process
func (m *Migrate) Up() error {
	if err := m.migorm.Migrate(); err != nil {
		return err
	}

	log.Printf("Migration did run successfully")

	return nil
}

//Rollback all migrations
func (m *Migrate) Rollback() error {
	for _, f := range m.list {
		if err := m.migorm.RollbackMigration(f); err != nil {
			return gormigrate.ErrRollbackImpossible
		}
	}

	log.Printf("Rollback migration did run successfully")

	return nil
}

//Rollback only last migration
func (m *Migrate) RollbackLas() error {
	if err := m.migorm.RollbackLast(); err != nil {
		return gormigrate.ErrRollbackImpossible
	}

	log.Printf("Rollback last migration did run successfully")

	return nil
}

//Rollback migration by uniq id. This identifier can be get
// from the migration file or from a table in the database
func (m *Migrate) RollbackTo(migrationId string) error {
	if err := m.migorm.RollbackTo(migrationId); err != nil {
		return gormigrate.ErrRollbackImpossible
	}

	log.Printf("Rollback migration" + migrationId + " did run successfully")

	return nil
}

//Rollback migration by migrations obj. Example: function GetMigration
// will return *gormigrate.Migration obj. Look migrations list for your path.
func (m *Migrate) RollbackMigration(migration *gormigrate.Migration) error {
	if err := m.migorm.RollbackMigration(migration); err != nil {
		return gormigrate.ErrRollbackImpossible
	}

	log.Printf("Rollback migration did run successfully")

	return nil
}
