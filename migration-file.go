package migorm

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

//Rule for create new migration
type Migration interface {
	TableName() string
	MigrationId() string
}

//Return migration for use in migrate process
func NewMigration(m Migration) *gormigrate.Migration {
	migrate := gormigrate.Migration{
		ID: m.MigrationId(),

		Migrate: func(tx *gorm.DB) error {
			return tx.AutoMigrate(m).Error
		},

		Rollback: func(tx *gorm.DB) error {
			return tx.DropTable((m).TableName()).Error
		},
	}

	return &migrate
}
